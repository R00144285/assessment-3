set DITA_HOME=C:\DITA\dita-ot-2.2.1
set ANT_HOME=%DITA_HOME%
set JAVA_HOME=C:\Program Files\Java\jre1.8.0_65
set PATH=%JAVA_HOME%\bin;%DITA_HOME%\bin;%ANT_HOME%\bin;%PATH%

set CLASSPATH=%DITA_HOME%\lib\saxon.jar;%DITA_HOME%\lib\saxon-dom.jar;%CLASSPATH%
set CLASSPATH=%DITA_HOME%\lib\dost.jar;%CLASSPATH%
set CLASSPATH=%DITA_HOME%\lib;%CLASSPATH%
set CLASSPATH=%DITA_HOME%\lib\commons-codec;%DITA_HOME%\lib\commons-io;%CLASSPATH%
set CLASSPATH=%DITA_HOME%\lib\xercesImpl.jar;%DITA_HOME%\lib\xml-apis.jar;%DITA_HOME%\lib\xml-resolver.jar;%CLASSPATH%

set CLASSPATH=%DITA_HOME%\plugins\com.katiebeatty.print-pdf1\lib\fo.jar;%CLASSPATH%

set ANT_OPTS=%ANT_OPTS% -Djavax.xml.transform.TransformerFactory=net.sf.saxon.TransformerFactoryImpl

DITA -f pdf -i Intro_to_Software.xml -o output